module ManageIQ::Providers::Aliyun::CloudManager::Vm::Operations::Guest
  extend ActiveSupport::Concern

  included do
    supports :reboot_guest do
      unsupported_reason_add(:reboot_guest, unsupported_reason(:control)) unless supports_control?
      unsupported_reason_add(:reboot_guest, _("The VM is not powered on")) unless current_state == "on"
    end
  end

  def raw_reboot_guest
    # https://help.aliyun.com/document_detail/25502.html?spm=5176.doc25501.6.698.HBDQHN
    with_provider_connection { |connection|
      parameters = {:InstanceId => ems_ref}
      connection.RebootInstance(parameters) # retrun {"RequestId"=>"9585CF9B-9049-41A1-B42B-BE3867C26AAB"}
    }
    # Temporarily update state for quick UI response until refresh comes along
    self.update_attributes!(:raw_power_state => "reboot") # show state as suspended
  end
end
