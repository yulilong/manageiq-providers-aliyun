module ManageIQ::Providers::Aliyun::CloudManager::Vm::Operations::Power
  extend ActiveSupport::Concern


  # def validate_pause
  #   validate_unsupported("Pause Operation")
  # end

  def raw_start
    # https://help.aliyun.com/document_detail/25500.html?spm=5176.doc25501.6.696.QQnRgO
    with_provider_connection { |connection|
      parameters = {:InstanceId => ems_ref}
      connection.StartInstance(parameters)
    }
    # Temporarily update state for quick UI response until refresh comes along
    self.update_attributes!(:raw_power_state => "powering_up")
  end

  def raw_stop
    # https://help.aliyun.com/document_detail/25501.html?spm=5176.doc25500.6.697.eSRybX
    with_provider_connection { |connection|
      parameters = {:InstanceId => ems_ref}
      connection.StopInstance(parameters)
    }
    # Temporarily update state for quick UI response until refresh comes along
    self.update_attributes!(:raw_power_state => "shutting_down")
  end
end
