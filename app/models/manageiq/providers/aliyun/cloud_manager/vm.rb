class ManageIQ::Providers::Aliyun::CloudManager::Vm < ManageIQ::Providers::CloudManager::Vm
  include_concern 'Operations'

  POWER_STATES = {
      "Running"       => "on",
      "Stopped"       => "off",
      "reboot"        => "reboot_in_progress",
      "powering_up"   => "powering_up",
      "shutting_down" => "powering_down",
      "Unknown"       => "unknown"
  }.freeze

  # setup power_state
  def self.calculate_power_state(raw_power_state)

    POWER_STATES[raw_power_state] || "terminated"
  end

end