# TODO: Separate collection from parsing (perhaps collecting in parallel a la RHEVM)

class ManageIQ::Providers::Aliyun::CloudManager::RefreshParser < ManageIQ::Providers::CloudManager::RefreshParser
  include ManageIQ::Providers::Aliyun::RefreshHelperMethods

  def self.ems_inv_to_hashes(ems, options = nil)
    new(ems, options).ems_inv_to_hashes
  end

  def initialize(ems, options = nil)
    @ems               = ems
    @connection        = ems.connect
    @data              = {}
    @data_index        = {}
    # @known_flavors     = Set.new
  end

  def ems_inv_to_hashes
    log_header = "MIQ(#{self.class.name}.#{__method__}) Collecting data for EMS name: [#{@ems.name}] id: [#{@ems.id}]"

    $fog_log.info("#{log_header}...")
    # The order of the below methods does matter, because there are inner dependencies of the data!

    get_flavors
    get_availability_zones
    get_instances


    $fog_log.info("#{log_header}...Complete")

    @data
  end

  private

  def get_flavors
    parameters = {}
    flavors = @connection.DescribeInstanceTypes(parameters)["InstanceTypes"]["InstanceType"]
    process_collection(flavors, :flavors) { |flavor| parse_flavor(flavor) }
  end

  # {
  #     "LocalStorageAmount": 1,
  #     "CpuCoreCount": 12,
  #     "InstanceTypeFamily": "ecs.gn3",
  #     "InstanceTypeId": "ecs.gn3.2xlarge",
  #     "LocalStorageCapacity": 350,
  #     "GPUSpec": "NVidia K2",
  #     "MemorySize": 24.0,
  #     "GPUAmount": 1
  # }
  def parse_flavor(flavor)
    uid = flavor["InstanceTypeId"]

    new_result = {
        :type                 => "ManageIQ::Providers::Aliyun::CloudManager::Flavor",
        :ems_ref              => uid,
        :name                 => uid,
        :cpus                 => flavor["CpuCoreCount"],
        :memory               => flavor["MemorySize"] * 1024 * 1024 * 1024  # unit:GB  1KB=1024b  1024KB=1mb 1024mb=1GB
    }
    return uid, new_result
  end

  def get_availability_zones
    parameters = {:RegionId => @ems.provider_region}
    zones = @connection.DescribeZones(parameters)["Zones"]["Zone"]
    process_collection(zones, :availability_zones) { |zone| parse_availability_zone(zone) }
  end

  # "AvailableResourceCreation": {}
  # "AvailableResources": {},
  # "AvailableInstanceTypes": {},
  # "ZoneId": "cn-shenzhen-b",
  # "LocalName": "华南 1 可用区 B",
  # "AvailableDiskCategories": {
  #   "DiskCategories": [
  #       "cloud_ssd",
  #       "cloud_efficiency"
  #   ]
  # }
  def parse_availability_zone(zone)
    uid = zone["ZoneId"]
    new_result = {
        :type    => ManageIQ::Providers::Aliyun::CloudManager::AvailabilityZone.name,
        :ems_ref => uid,
        :name    => zone["LocalName"]
    }
    return uid, new_result

  end

  def instance_groups
    page_number = 1
    instance_group = []

    loop do
      parameters = {:RegionId => @ems.provider_region, :PageNumber => page_number }
      instances = @connection.DescribeInstances(parameters)
      instance_group.concat(instances["Instances"]["Instance"])
      break if page_number * instances["PageSize"] > instances["TotalCount"]
      page_number += 1
    end

    instance_group
  end

  def get_instances
    process_collection(instance_groups, :vms) { |instance| parse_instance(instance) }
  end


  def parse_instance(instance)

    status = instance["Status"] || "UNKNOWN"
    uid = instance["InstanceId"]
    name = instance["HostName"]
    name = uid if name.blank?

    flavor_uid = instance["InstanceType"]
    # @known_flavors << flavor_uid
    flavor = @data_index.fetch_path(:flavors, flavor_uid)


    public_ip = instance["PublicIpAddress"]["IpAddress"][0]
    public_ip = instance["EipAddress"]["IpAddress"] if public_ip.blank?
    public_network = {
        :ipaddress => public_ip
    }.delete_nils
    private_network = {
        :ipaddress => instance["VpcAttributes"]["PrivateIpAddress"]["IpAddress"][0]
    }.delete_nils

    new_result = {
        :type                => ManageIQ::Providers::Aliyun::CloudManager::Vm.name,
        :uid_ems             => uid,
        :ems_ref             => uid,
        :name                => name,
        :vendor              => "aliyun",
        :raw_power_state     => status,
        :connection_state    => "connected",

        :hardware            => {
            :cpu_sockets          => flavor[:cpus],
            :cpu_cores_per_socket => 1,
            :cpu_total_cores      => flavor[:cpus],
            :memory_mb            => flavor[:memory] / 1.megabyte,
            :disks                => [], # Filled in later conditionally on flavor
            :networks             => [], # Filled in later conditionally on what's available
        },

        :availability_zone   => @data_index.fetch_path(:availability_zones, instance["ZoneId"]),
        :flavor              => flavor,
    }

    new_result[:hardware][:networks] << private_network.merge(:description => "private") unless private_network.blank?
    new_result[:hardware][:networks] << public_network.merge(:description => "public")   unless public_network.blank?


    # manageiq/app/models/manageiq/providers/cloud_manager/refresh_parser.rb
    # line: 41  def add_instance_disk(disks, size, location, name, controller_type)

    return uid, new_result
  end



end