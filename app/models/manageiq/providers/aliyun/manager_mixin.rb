module ManageIQ::Providers::Aliyun::ManagerMixin
  extend ActiveSupport::Concern # if not add this line,then module ClassMethods is Invalid

  def description
    ManageIQ::Providers::Aliyun::Regions.find_by_name(provider_region)[:description]
  end

  def connect(options = {})
    raise "no credentials defined" if missing_credentials?(options[:auth_type])

    access_key_id = options[:user] || authentication_userid(options[:auth_type])
    access_key_secret = options[:pass] || authentication_password(options[:auth_type])

    self.class.raw_connect(access_key_id, access_key_secret)
  end

  def verify_credentials(auth_type = nil, options = {})
    raise MiqException::MiqHostError, "No credentials defined" if missing_credentials?(auth_type)

    begin
      with_provider_connection(options.merge(:auth_type => auth_type)) do |aliyun|
        aliyun.DescribeRegions({})
      end
    rescue => err
      _log.error("Error Class=#{err.class.name}, Message=#{err.message}")
      raise(err)
    end

    true
  end


  module ClassMethods

    #
    # Connections
    #
    def raw_connect(access_key_id, access_key_secret)
      require 'rubygems'
      require 'aliyun_ruby_api'

      options = {:access_key_id => access_key_id,
                 :access_key_secret => access_key_secret,
                 :endpoint_url => "https://ecs.aliyuncs.com/"}

      Aliyun::Service.new options
    end
  end
end