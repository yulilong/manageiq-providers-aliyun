module ManageIQ::Providers::Aliyun::RefreshHelperMethods


  def process_collection(collection, key)
    @data[key] ||= []
    return if collection.nil?

    collection.each do |item|
      uid, new_result = yield(item)

      @data[key] << new_result
      @data_index.store_path(key, uid, new_result)
    end
  end

end