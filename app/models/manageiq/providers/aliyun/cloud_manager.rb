class ManageIQ::Providers::Aliyun::CloudManager < ManageIQ::Providers::CloudManager
  require_nested :MetricsCollectorWorker
  require_nested :RefreshParser
  require_nested :RefreshWorker
  require_nested :Refresher
  require_nested :Flavor
  require_nested :AvailabilityZone
  require_nested :Vm


  include ManageIQ::Providers::Aliyun::ManagerMixin
  OrchestrationTemplateCfn.register_eligible_manager(self)


  has_one :network_manager,
          :foreign_key => :parent_ems_id,
          :class_name  => ManageIQ::Providers::Aliyun::NetworkManager.name,
          :autosave    => true,
          :dependent   => :destroy

  before_validation :ensure_managers

  supports :regions


  def ensure_network_manager
    build_network_manager(:type => ManageIQ::Providers::Aliyun::NetworkManager.name) unless network_manager
  end

  def ensure_managers
    build_network_manager unless network_manager
    network_manager.name            = "#{name} Network Manager"
    network_manager.zone_id         = zone_id
    network_manager.provider_region = provider_region
  end

  def self.ems_type
    @ems_type ||= "aliyun".freeze
  end

  def self.description
    @description ||= "Aliyun ECS".freeze
  end

  # if not this method, then gems/activerecord-5.0.0.1/lib/active_record/validations.rb
  # <Method: ManageIQ::Providers::Aliyun::CloudManager(ActiveRecord::Validations)#valid?>
  # return false
  # manageiq/app/controllers/mixins/ems_common_angular.rb
  # line:91
  def self.hostname_required?
    false
  end


end
