class ManageIQ::Providers::Aliyun::NetworkManager < ManageIQ::Providers::NetworkManager
  require_nested :MetricsCollectorWorker
  require_nested :RefreshParser
  require_nested :RefreshWorker
  require_nested :Refresher
  require_nested :SecurityGroup


  include ManageIQ::Providers::Aliyun::ManagerMixin

  # Auth and endpoints delegations, editing of this type of manager must be disabled
  delegate :authentication_check,
           :authentication_status,
           :authentication_status_ok?,
           :authentications,
           :authentication_for_summary,
           :zone,
           :connect,
           :verify_credentials,
           :with_provider_connection,
           :address,
           :ip_address,
           :hostname,
           :default_endpoint,
           :endpoints,
           :to        => :parent_manager,
           :allow_nil => true

  def self.ems_type
    @ems_type ||= "aliyun_network".freeze
  end

  def self.description
    @description ||= "Aliyun ECS Network".freeze
  end

  # if not this method, then gems/activerecord-5.0.0.1/lib/active_record/validations.rb
  # <Method: ManageIQ::Providers::Aliyun::CloudManager(ActiveRecord::Validations)#valid?>
  # return false
  # manageiq/app/controllers/mixins/ems_common_angular.rb
  # line:91
  def self.hostname_required?
    false
  end

end