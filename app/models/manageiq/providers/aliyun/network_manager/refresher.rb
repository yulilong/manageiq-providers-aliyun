module ManageIQ::Providers
  class Aliyun::NetworkManager::Refresher < ManageIQ::Providers::BaseManager::Refresher
    include ::EmsRefresh::Refreshers::EmsRefresherMixin

    def parse_legacy_inventory(ems)

      ::ManageIQ::Providers::Aliyun::NetworkManager::RefreshParser.ems_inv_to_hashes(ems)
    end

    def post_process_refresh_classes
      []
    end
  end
end