class ManageIQ::Providers::Aliyun::NetworkManager::RefreshWorker < ::MiqEmsRefreshWorker
  require_nested :Runner

  def self.ems_class
    ManageIQ::Providers::Aliyun::NetworkManager
  end

  def self.settings_name
    :ems_refresh_worker_aliyun_network
  end
end
