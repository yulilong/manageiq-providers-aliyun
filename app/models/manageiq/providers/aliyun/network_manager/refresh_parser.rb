# TODO: Separate collection from parsing (perhaps collecting in parallel a la RHEVM)

class ManageIQ::Providers::Aliyun::NetworkManager::RefreshParser
  include ManageIQ::Providers::Aliyun::RefreshHelperMethods

  def self.ems_inv_to_hashes(ems, options = nil)
    new(ems, options).ems_inv_to_hashes
  end

  def initialize(ems, options = nil)
    @ems               = ems
    @connection        = ems.connect
    @data              = {}
    @data_index        = {}
  end

  def ems_inv_to_hashes
    log_header = "MIQ(#{self.class.name}.#{__method__}) Collecting data for EMS name: [#{@ems.name}] id: [#{@ems.id}]"

    $fog_log.info("#{log_header}...")
    # The order of the below methods does matter, because there are inner dependencies of the data!

    get_security_groups


    $fog_log.info("#{log_header}...Complete")

    @data
  end

  private

  def security_groups
    page_number = 1
    security_group = []

    loop do
      parameters = {:RegionId => @ems.provider_region, :PageNumber => page_number }
      security = @connection.DescribeSecurityGroups(parameters)
      security_group.concat(security["SecurityGroups"]["SecurityGroup"])
      break if page_number * security["PageSize"] > security["TotalCount"]
      page_number += 1
    end

    security_group
  end

  def get_security_groups
    process_collection(security_groups, :security_groups) { |sg| parse_security_group(sg) }
    # get_firewall_rules
  end

  # {
  #     "CreationTime": "2016-12-14T01:25:29Z",
  #     "Tags": {
  #         "Tag": [
  #
  #         ]
  #     },
  #     "SecurityGroupId": "sg-wz90y7pe0g08ts9qjj9z",
  #     "Description": "",
  #     "SecurityGroupName": "test5",
  #     "AvailableInstanceAmount": 1000,
  #     "VpcId": "vpc-wz9mizc198s6m6fpvisb2"
  # }
  def parse_security_group(sg)
    uid = sg["SecurityGroupId"]

    new_result = {
        :type                => ManageIQ::Providers::Aliyun::NetworkManager::SecurityGroup.name,
        :ems_ref             => uid,
        :name                => sg["SecurityGroupName"],
        :description         => sg["Description"]
    }

    return uid, new_result
  end




end