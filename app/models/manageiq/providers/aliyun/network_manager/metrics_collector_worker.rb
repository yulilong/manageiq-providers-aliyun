class ManageIQ::Providers::Aliyun::NetworkManager::MetricsCollectorWorker < ::MiqEmsMetricsCollectorWorker
  require_nested :Runner

  self.default_queue_name = "aliyun_network"

  def friendly_name
    @friendly_name ||= "C&U Metrics Collector for Aliyun Network"
  end

  def self.ems_class
    ManageIQ::Providers::Aliyun::NetworkManager
  end

  def self.settings_name
    :ems_metrics_collector_worker_aliyun_network
  end
end
