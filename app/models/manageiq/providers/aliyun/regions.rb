# https://github.com/qjpcpu/aliyun-api/blob/master/README.md#调用ecs
# ecs.describe_regions {}

module ManageIQ
  module Providers::Aliyun
    module Regions
      # options = {:access_key_id => "LTAIrR4BclgVxxxx",
      #            :access_key_secret => "iWXEZWoKXzAbNgEdkGxOIqAMqkxxxx",
      #            :endpoint_url => "https://ecs.aliyuncs.com/"}
      # service = Aliyun::Service.new options
      # parameters = {}
      # service.DescribeRegions(parameters)
      REGIONS = {
          "cn-shenzhen"      => {
              :name        => "cn-shenzhen",
              :description => "华南 1(cn-shenzhen)",
          },
          "cn-qingdao"      => {
              :name        => "cn-qingdao",
              :description => "华北 1(cn-qingdao)",
          },
          "cn-beijing"      => {
              :name        => "cn-beijing",
              :description => "华北 2(cn-beijing)",
          },
          "cn-hangzhou"      => {
              :name        => "cn-hangzhou",
              :description => "华东 1(cn-hangzhou)",
          },
          "cn-shanghai"      => {
              :name        => "cn-shanghai",
              :description => "华东 2(cn-shanghai)",
          },
          "ap-southeast-1"      => {
              :name        => "ap-southeast-1",
              :description => "亚太东南 1 (新加坡)",
          },
          "us-east-1"      => {
              :name        => "us-east-1",
              :description => "美国东部 1 (弗吉尼亚)",
          },
          "cn-hongkong"      => {
              :name        => "cn-hongkong",
              :description => "香港",
          },
          "me-east-1"      => {
              :name        => "me-east-1",
              :description => "中东东部 1 (迪拜)",
          },
          "eu-central-1"      => {
              :name        => "eu-central-1",
              :description => "欧洲中部 1 (法兰克福)",
          },
          "ap-northeast-1"      => {
              :name        => "ap-northeast-1",
              :description => "亚太东北 1 (日本)",
          },
          "us-west-1"      => {
              :name        => "us-west-1",
              :description => "美国西部 1 (硅谷)",
          },
      }

      def self.regions
        REGIONS
      end

      def self.all
        regions.values
      end

      def self.find_by_name(name)
        regions[name]
      end

    end

  end

end
