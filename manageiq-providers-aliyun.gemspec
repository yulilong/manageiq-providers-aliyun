$:.push File.expand_path("../lib", __FILE__)

require "manageiq/providers/aliyun/version"

Gem::Specification.new do |s|
  s.name        = "manageiq-providers-aliyun"
  s.version     = ManageIQ::Providers::Aliyun::VERSION
  s.authors     = ["yulilong"]
  s.homepage    = "https://bitbucket.org/yulilong/manageiq-providers-aliyun"
  s.summary     = "aliyun Provider for ManageIQ"
  s.description = "aliyun Provider for ManageIQ"
  s.licenses    = ["Apache-2.0"]

  s.files = Dir["{app,config.lib}/**/*"]

  s.add_dependency("aliyun_ruby_api")
end
