module ManageIQ
  module Providers
    module Aliyun
      class Engine < ::Rails::Engine
        isolate_namespace ManageIQ::Providers::Aliyun
      end
    end
  end
end

